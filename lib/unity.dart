import 'package:flutter/material.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';

class UnityPage extends StatefulWidget {
  @override
  _UnityPageState createState() => _UnityPageState();
}

class _UnityPageState extends State<UnityPage> {
  UnityWidgetController _unityWidgetController;
  get onUnityMessage => null;

  @override
  void initState() {
    super.initState();
  }

  void dispose() {
    super.dispose();
    _unityWidgetController.unload();
    _unityWidgetController.quit();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Unity Flutter AR'),
        ),
        body: Container(
          color: Colors.indigo,
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.6,
                child: UnityWidget(
                  onUnityCreated: onUnityCreated,
                  onUnityMessage: onUnityMessage,
                  isARScene: true,
                  fullscreen: false,
                ),
              ),
              RaisedButton(
                color: Colors.grey[800],
                onPressed: () {
                  setKacamata('1');
                },
                child: Text(
                  "Juventus",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              RaisedButton(
                color: Colors.orange,
                onPressed: () {
                  setKacamata('0');
                },
                child: Text(
                  "Kangaroo",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onUnityCreated(controller) {
    _unityWidgetController = controller;
  }

  void setKacamata(String kacamata) {
    print("________________>>>>>");
    print(kacamata);
    print("________________>>>>>");
    try {
      _unityWidgetController.postMessage(
        'SelectionAR',
        'setKacamata',
        kacamata,
      );
    } catch (e) {
      print(e);
    }
    print(kacamata);
    print("________________<<<");
  }
}
