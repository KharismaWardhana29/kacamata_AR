﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionAR : MonoBehaviour
{
    public GameObject kacamataJuventus;

    public GameObject kacamataKangoroo;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    void setKacamata(string kacamataId)
    {
        print("Unity checked------->>>");
        print (kacamataId);
        bool configJuve = false;
        bool configKangoroo = true;
        if (kacamataId == "1")
        {
            configJuve = true;
            configKangoroo = false;
        }
        kacamataJuventus.SetActive (configJuve);
        kacamataKangoroo.SetActive (configKangoroo);
        print("Unity checked------->>>");
    }
}
